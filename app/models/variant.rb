class Variant < ApplicationRecord
  belongs_to :product
  has_many :assets, :dependent => :delete_all
end
