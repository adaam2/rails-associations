Product.delete_all

max_products = 200
variants_per_product = 3
assets_per_variant = 4

1.upto(max_products).each do |i| # Products
	variants = []
	1.upto(variants_per_product).each do |j| # Variants
		assets = []
		1.upto(assets_per_variant).each do |k| # Assets
			if k % 2
				image = Image.create(title: Faker::Name.name, url: Faker::Avatar.image, description: Faker::Hipster.paragraph)

				assets.push image
			else
				video = Video.create(title: Faker::Name.name, url: Faker::Avatar.image, description: Faker::Hipster.paragraph)

				assets.push video
			end
		end

		variant = Variant.create(:title => Faker::Name.name, :description => Faker::Hipster.paragraph, :published => Faker::Boolean.boolean, :expiry_date => Faker::Time.between(DateTime.now, DateTime.now + 1000), :assets => assets)

		variants.push variant
	end
	product = Product.create(title: Faker::Name.name, description: Faker::Hipster.paragraph, variants: variants, published: Faker::Boolean.boolean)
end