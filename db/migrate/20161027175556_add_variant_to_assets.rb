class AddVariantToAssets < ActiveRecord::Migration[5.0]
  def change
    add_reference :assets, :variant, foreign_key: true
  end
end
