class AddCascadeToTables < ActiveRecord::Migration[5.0]
  def change
  	remove_foreign_key :variants, :products
  	add_foreign_key :variants, :products, on_delete: :cascade
  	remove_foreign_key :assets, :variants
  	add_foreign_key :assets, :variants, on_delete: :cascade
  end
end
