class AddUrlToAssets < ActiveRecord::Migration[5.0]
  def change
    add_column :assets, :url, :string
  end
end
