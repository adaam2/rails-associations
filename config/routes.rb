Rails.application.routes.draw do
  resources :products
  resources :assets
  root 'products#index'
end
